﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TaskManager.Web.Models
{
    public enum ToDoUrgency
    {
        [Display(Name = "Due")] Due,
        [Display(Name = "Upcoming")] Upcoming,
        [Display(Name = "Past Due")] PastDue
    }
    public enum ToDoStatus
    {
        [Display(Name = "To Do")] ToDo,
        [Display(Name = "In Progress")] InProgress,
        [Display(Name = "Done")] Done
    }
    public class ToDo
    {
        protected virtual DateTime GetDateTime()
        {
            return DateTime.Now;
        }
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DueDate { get; set; }
        public string Notes { get; set; }
        public ToDoStatus Status { get; set; }
        public ToDoUrgency Urgency
        {
            get
            {
                DateTime now = GetDateTime();
                DateTime inTwoWeeks = now.AddDays(14);
                if (this.DueDate < now)
                    return ToDoUrgency.PastDue;
                else if (this.DueDate < inTwoWeeks)
                    return ToDoUrgency.Upcoming;
                else
                    return ToDoUrgency.Due;
            }
        }
        public String DisplayDueDate
        {
            get
            {
                return this.DueDate.ToString("M/d/yy");
            }
        }
    }
}
