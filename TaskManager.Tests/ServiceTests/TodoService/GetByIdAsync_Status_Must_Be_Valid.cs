﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManager.Api;
using TaskManager.Data.Framework;
using TaskManager.Data.Models;

namespace TaskManager.Tests.ServiceTests.TodoService
{
    [TestFixture]
    class GetByIdAsync_Status_Must_Be_Valid
    {
        private readonly AppDbContext _db;
        private ToDo _todo;
        public GetByIdAsync_Status_Must_Be_Valid()
        {
            string[] empty = { };
            IServiceProvider services = ApiProgram.CreateHostBuilder(empty).Build().Services;
            _db = (AppDbContext)services.GetService(typeof(AppDbContext));
        }
        [OneTimeTearDown]
        public void DerivedTearDown()
        {
            _db.ToDo.Remove(_todo);
            _db.SaveChanges();
        }
        [Test]
        public void TestStatusMustBeValid()
        {
            Assert.Throws<DbUpdateException>(() =>
            {
                _todo = new ToDo
                {
                    DueDate = DateTime.MaxValue,
                    Description = Guid.NewGuid().ToString(),
                    Notes = Guid.NewGuid().ToString(),
                    Title = Guid.NewGuid().ToString(),
                    Status = (ToDoStatus)3
                };
                _db.ToDo.Add(_todo);
                _db.SaveChanges();
            });
        }
    }
}
