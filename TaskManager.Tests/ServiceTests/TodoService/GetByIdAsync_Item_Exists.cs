using NUnit.Framework;
using TaskManager.Data.Models;
using TaskManager.Services.Services;
using System;
using SpecsFor.StructureMap;
using TaskManager.Common.Contracts.ToDo;
using TaskManager.Api;
using TaskManager.Data.Framework;

namespace TaskManager.Tests
{
    [TestFixture]
    public class GetByIdAsync_Item_Exists : SpecsFor<ToDoService>
    {
        private readonly ToDoService _sut;
        private readonly AppDbContext _db;
        private ToDo _todo;
        private ToDoDto _todoItem;

        public GetByIdAsync_Item_Exists()
        {
            string[] empty = { };
            IServiceProvider services = ApiProgram.CreateHostBuilder(empty).Build().Services;
            _sut = (ToDoService)services.GetService(typeof(IToDoService));
            _db = (AppDbContext)services.GetService(typeof(AppDbContext));
        }

        protected override void Given()
        {
            base.Given();
            _todo = new ToDo
            {
                DueDate = DateTime.MaxValue,
                Description = Guid.NewGuid().ToString(),
                Notes = Guid.NewGuid().ToString(),
                Title = Guid.NewGuid().ToString(),
                Status = 0
            };
            _db.ToDo.Add(_todo);
            _db.SaveChanges();
        }

        protected override void When()
        {
            base.When();
            _todoItem = _sut.GetAsync(_todo.Id).Result;
        }

        [OneTimeTearDown]
        public void DerivedTearDown()
        {
            _db.ToDo.Remove(_todo);
            _db.SaveChanges();
        }

        [Test]
        public void Then_An_Object_With_A_Matching_Id_Is_Returned()
        {
            Assert.NotNull(_todoItem);
            Assert.AreEqual(_todo.Id, _todoItem.Id);
        }

        [Test]
        public void Then_An_Object_With_A_Status_Is_Returned()
        {
            Assert.NotNull(_todoItem.Status);
        }

    }
}