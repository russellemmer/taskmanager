﻿using NUnit.Framework;
using System;
using TaskManager.Web.Models;

namespace TaskManager.Tests.WebTests.Models
{
    public class ToDoTestStub : ToDo
    {
        private readonly DateTime _dateTime;
        public ToDoTestStub(DateTime dateTime)
        {
            _dateTime = dateTime;
        }
        protected override DateTime GetDateTime()
        {
            return _dateTime;
        }
    }

    [TestFixture]
    class ToDoTest
    {
        private DateTime _now;
        [OneTimeSetUp]
        public void SetNow()
        {
            _now = new DateTime(2021, 1, 9);
        }
        [Test]
        public void ToDoHasUrgency()
        {
            ToDo todo = new ToDoTestStub(_now)
            {
                DueDate = new DateTime(2021, 2, 2),
                Description = Guid.NewGuid().ToString(),
                Notes = Guid.NewGuid().ToString(),
                Title = Guid.NewGuid().ToString(),
                Status = 0
            };

            Assert.AreEqual(ToDoUrgency.Due, todo.Urgency);
        }

        [Test]
        public void ToDoMayBePastDue()
        {
            ToDo todo = new ToDoTestStub(_now)
            {
                DueDate = new DateTime(2021, 1, 1),
                Description = Guid.NewGuid().ToString(),
                Notes = Guid.NewGuid().ToString(),
                Title = Guid.NewGuid().ToString(),
                Status = 0
            };

            Assert.AreEqual(ToDoUrgency.PastDue, todo.Urgency);
        }

        [Test]
        public void ToDoMayBeUpcoming()
        {
            ToDo todo = new ToDoTestStub(_now)
            {
                DueDate = new DateTime(2021, 1, 22),
                Description = Guid.NewGuid().ToString(),
                Notes = Guid.NewGuid().ToString(),
                Title = Guid.NewGuid().ToString(),
                Status = 0
            };

            Assert.AreEqual(ToDoUrgency.Upcoming, todo.Urgency);
        }
    }
}
