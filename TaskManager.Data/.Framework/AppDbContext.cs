﻿using Microsoft.EntityFrameworkCore;
using TaskManager.Data.Models;

namespace TaskManager.Data.Framework
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        public DbSet<ToDo> ToDo { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder
                .UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=AppDbContext-ced5cbee-7432-4c1d-9ac2-ec59d54c6bcd;Trusted_Connection=True;MultipleActiveResultSets=true")
                .UseEnumCheckConstraints();
    }
}
